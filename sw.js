const cacheName = 'JopJumps';
var filesToCache = [
  '/',
  'index.html',
  'style.css',
  'src/animations.js',
  'src/anime.min.js',
  'src/app.js?rev=1',
  'src/cards.js',
  'src/data.js',
  'src/localdata.js',
];
self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
});
self.addEventListener('activate',  event => {
  event.waitUntil(self.clients.claim());
});
self.addEventListener('fetch', event => {
  event.respondWith(
    caches.match(event.request, {ignoreSearch:true}).then(response => {
      return response || fetch(event.request);
    })
  );
});