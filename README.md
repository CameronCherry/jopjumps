# JopJumps

## Description

JopJumps is a JavaScript implementation of a popular game of a [similar name](https://en.wikipedia.org/wiki/Top_Trumps).

## Current features

- Play against the computer
- Use your own local data file
- Progressive Web Application (PWA) support
- Support for both Firefox- and Chromium-based browsers
- Light mode, dark mode and [Gruvbox](https://github.com/morhetz/gruvbox) mode

## Planned features

- Custom colour themes
- Variable difficulty levels
- Visual / UX enhancements
- Custom image support
- Custom attributes for local data files
- Manual input and amendment to data
- Enhanced rule explanations
- Walkthrough
- Multiplayer
- Safari support

## How to run

### Option 1 - Play online (Recommended)

Play JopJumps online here:  https://cameroncherry.com/webprojects/jopjumps

The above website link is hosted by Gitlab and guarantees that you will be playing the latest released version.

The above link can also be installed on your machine as a Progressive Web App (PWA).


### Option 2 - Play locally (Not recommended)

Download the `local-index.html` file and open this in your favourite web browser.

Note that this file will not auto-update, and so you will need to re-download the file to experience future JopJumps updates.

### Option 3 - Build locally

*(The below method assumes that Python and pip are already installed on your machine.)*

Open a terminal and follow the below:

```
pip3 install live-server

git clone https://gitlab.com/CameronCherry/jopjumps.git

cd jopjumps

live-server ../index.html
```

## License

Developed by Cameron Cherry under the MIT license (see the LICENSE file for more information).