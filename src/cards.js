import { refreshData } from "./data.js";

export default class Deck {
	constructor(cards = freshDeck()) {
		this.cards = cards;
	}

	get numberOfCards() {
		return this.cards.length;
	}

	pop() {
		return this.cards.shift();
	}

	push(card) {
		this.cards.push(card);
	}

	shuffle() {
		for (let i = this.numberOfCards - 1; i > 0; i--) {
			const newIndex = Math.floor(Math.random() * (i + 1));
			const oldValue = this.cards[newIndex];
			this.cards[newIndex] = this.cards[i];
			this.cards[i] = oldValue;
		}
	}
}

class Card {
	constructor(cardname, gamesPlayed, winPerc, winStreak, debut) {
		this.cardname = cardname;
		this.gamesPlayed = gamesPlayed;
		this.winPerc = winPerc;
		this.winStreak = winStreak;
		this.debut = debut;
	}

	getHTML(isCpuCard = false) {
		const cardDiv = document.createElement("div");
		cardDiv.classList.add("card");
		if (isCpuCard) {
			cardDiv.classList.add("cpuCard");
			cardDiv.id = "cpuCard";
		} else {
			cardDiv.classList.add("p1Card");
			cardDiv.id = "p1Card";
		}
		cardDiv.innerHTML = "😀";
		cardDiv.dataset.value = `${this.cardname}\n${this.gamesPlayed}\n${this.winPerc}\%\n${this.winStreak}\n${this.debut}`;
		cardDiv.dataset.attribs = `Name:\nGames:\nWin \%:\nWin streak:\nDebut:`;
		return cardDiv;
	}

	// https://stackoverflow.com/questions/2532218/pick-random-property-from-a-javascript-object
	randomProperty() {
		let keys = Object.keys(this);
		let k = keys[((keys.length - 1) * Math.random() + 1) << 0];
		return { attrib: k, attribVal: this[k] };
	}
}

function freshDeck() {
	let vals = [];
	const DATA = refreshData();
	const NUM_CARDS = DATA.values.length;
	for (let i = NUM_CARDS - 1; i >= 1; i--) {
		vals.push(
			new Card(
				DATA.values[i][0], // Name
				parseInt(DATA.values[i][1]), // Games played
				Number(DATA.values[i][2]), // Win perc
				parseInt(DATA.values[i][3]), // Win streak
				DATA.values[i][4] // Debut
			)
		);
	}
	return vals;
}
