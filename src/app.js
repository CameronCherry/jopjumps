import Deck from "./cards.js";
import {
	playAnimation,
	growShrinkRestartButton,
	growShrinkNextCardButton,
} from "./animations.js";

// https://cdn.jsdelivr.net/npm/papaparse@5.3.0/papaparse.min.js

const cpuCardSlot = document.getElementById("cpuCardSlot");
const p1CardSlot = document.getElementById("p1CardSlot");
const cpuDeckElement = document.querySelector(".cpuDeck");
const p1DeckElement = document.querySelector(".p1Deck");
const outcomeText = document.getElementById("outcomeText");
const historyText = document.getElementById("historyText");
const history = document.querySelector(".historyItems");
const playerOptionElements = document.getElementsByClassName("p1Option");
const restartButton = document.getElementById("restartButton");
const nextCardButton = document.getElementById("nextCardButton");
const nextCardButtonContainer = document.getElementById("nextCardContainer");
const p1Options = document.getElementById("p1Options");
const optionsHeader = document.getElementById("optionsHeader");
const buttonContainer = document.querySelector(".container.button");
const historyContainer = document.querySelector(".container.history");
const showHistoryContainer = document.querySelector(".container.showhistory");
const showHistoryButton = document.getElementById("showHistoryButton");
const closeHistoryButton = document.getElementById("closeHistoryButton");
const overlayElement = document.getElementById("overlay");

let p1Deck,
	cpuDeck,
	p1Card,
	cpuCard,
	inRound,
	attribPlayed,
	isStop,
	player1Turn,
	waitingForNextCardPress;

// https://stackoverflow.com/questions/49580666/check-if-an-css-animation-is-completed-with-jquery-or-js

function startGame() {
	setUpGame();
	playGame();
}

function onButtonClick(event) {
	if (isStop) {
		return;
	}
	attribPlayed = event.target.value;
	playerPlayCard();
}

function setUpGame() {
	for (let i = 0; i < playerOptionElements.length; i++) {
		playerOptionElements[i].addEventListener("click", (event) => {
			onButtonClick(event);
		});
	}
	restartButton.addEventListener("click", reloadGame);
	nextCardButton.addEventListener("click", showPlayerNextCard);
	let historyItems = document.getElementsByClassName("historyItem");
	for (let i = 0; i < historyItems.length; i++) {
		historyItems[i].parentNode.removeChild(historyItems[i]);
	}
	const showHistoryPopupElems = [showHistoryButton, overlayElement, closeHistoryButton];
	for (let i = 0; i < showHistoryPopupElems.length; i++) {
		showHistoryPopupElems[i].addEventListener("click", showHideHistoryPopup);
	}
	makeOptionsActive();
	player1Turn = true;
}

function playGame() {
	const deck = new Deck();
	deck.shuffle();

	const deckMidpoint = Math.ceil(deck.numberOfCards / 2);
	p1Deck = new Deck(deck.cards.slice(0, deckMidpoint));
	cpuDeck = new Deck(deck.cards.slice(deckMidpoint, deck.numberOfCards));
	inRound = false;
	isStop = false;

	cleanBeforeRound();
	showPlayerNextCard();
	waitingForNextCardPress = false;
}

function cleanBeforeRound() {
	inRound = false;
	cpuCardSlot.innerHTML = "";
	p1CardSlot.innerHTML = "";
	outcomeText.innerText = "Waiting for Player 1...";
	historyText.classList.toggle("hidden");

	updateDeckCount();
}

function flipCards() {
	updateDeckCount();
	if (player1Turn) {
		showCPUCard();
	} else {
		playAnimation();
		showCPUCard();
	}

	let outcome;

	if (isRoundWinner(p1Card, cpuCard)) {
		outcome = "Win";
		outcomeText.innerText = "You win this round...";
		p1Deck.push(p1Card);
		p1Deck.push(cpuCard);
	} else if (isRoundWinner(cpuCard, p1Card)) {
		outcome = "Lose";
		outcomeText.innerText = "This round goes to CPU...";
		cpuDeck.push(p1Card);
		cpuDeck.push(cpuCard);
	} else {
		outcome = "Draw";
		outcomeText.innerText = "It's a draw";
		p1Deck.push(p1Card);
		cpuDeck.push(cpuCard);
	}

	updateHistory(player1Turn, outcome);

	if (isGameOver(p1Deck)) {
		outcomeText.innerText = "You Lose!!";
		isStop = true;
		updateHistoryForGameEnd(true);
		growShrinkRestartButton();
	} else if (isGameOver(cpuDeck)) {
		outcomeText.innerText = "You Win!!";
		isStop = true;
		updateHistoryForGameEnd(false);
		growShrinkRestartButton();
	} else {
		player1Turn = !player1Turn;
		inRound = false;
	}
}

function playerPlayCard() {
	makeOptionsInactive();
	if (isStop || waitingForNextCardPress || !player1Turn) return;
	flipCards();
}

function cpuPlayCard() {
	if (isStop || waitingForNextCardPress || player1Turn) return;
	attribPlayed = randomAttribute();
	flipCards();
	waitingForNextCardPress = true;
}

function randomAttribute() {
	return cpuCard.randomProperty().attrib;
}

function updateDeckCount() {
	cpuDeckElement.innerText = cpuDeck.numberOfCards + 1;
	p1DeckElement.innerText = p1Deck.numberOfCards;
}

function isRoundWinner(cardOne, cardTwo) {
	if (attribPlayed === "gamesPlayed") {
		return cardOne.gamesPlayed > cardTwo.gamesPlayed;
	} else if (attribPlayed === "winPerc") {
		return cardOne.winPerc > cardTwo.winPerc;
	} else if (attribPlayed === "winStreak") {
		return cardOne.winStreak > cardTwo.winStreak;
	} else if (attribPlayed === "debut") {
		return cardOne.debut < cardTwo.debut;
	} else {
		alert("Error trying to play attribute ", attribPlayed);
	}
}

function isGameOver(deck) {
	return deck.numberOfCards === 0;
}

function updateHistory(player1Turn = player1Turn, outcome) {
	const historyItemHTML = document.createElement("div");
	let space = "&ensp;";
	let prefix = "<span class=\"CPUHistoryText\">CPU</span>" + space + "played" + space;
	let suffix = space + "and lost!";
	let action, p1Value, cpuValue;
	if (player1Turn) {
		prefix = "<span class=\"p1HistoryText\">You</span>" + space + "played" + space;
	}
	let player1Win = (outcome === "Win");
	let draw = (outcome === "Draw");
	let cpuWin = ((!player1Win) && (!draw));
	if ((player1Turn && player1Win) || (!player1Turn && cpuWin)) {
		suffix = space + "and won!";
	} else if (draw) {
		suffix = space + "and it was a draw!";
	}
	switch (attribPlayed) {
		case "gamesPlayed":
			action = "Games played";
			p1Value = p1Card.gamesPlayed;
			cpuValue = cpuCard.gamesPlayed;
			break;
		case "winPerc":
			action = "Win percentage";
			p1Value = p1Card.winPerc + "%";
			cpuValue = cpuCard.winPerc + "%";
			break;
		case "winStreak":
			action = "Win streak";
			p1Value = p1Card.winStreak;
			cpuValue = cpuCard.winStreak;
			break;
		case "debut":
			action = "Debut";
			p1Value = p1Card.debut;
			cpuValue = cpuCard.debut;
			break;
	}
	let inner = prefix + "<span class=\"actionPlayed\">" + action + "</span>" + suffix;
	historyText.innerHTML = inner;
	historyText.classList.toggle("hidden");
	historyItemHTML.innerHTML = inner;
	historyItemHTML.classList.add("historyItem");
	historyItemHTML.title = "You: " + p1Value + "; CPU: " + cpuValue;
	if (cpuWin) {
		historyItemHTML.classList.add("cpu");
	} else if (draw) {
		historyItemHTML.classList.add("draw");
	}

	history.appendChild(historyItemHTML);
	scrollHistoryToBottom();
}

function updateHistoryForGameEnd(isCpuWin) {
	const historyItemHTML = document.createElement("div");
	historyItemHTML.innerText = "That's the end - you won!";
	historyItemHTML.classList.add("historyItem", "endgame");
	if (isCpuWin) {
		historyItemHTML.innerText = "That's the end - you lost!";
		historyItemHTML.classList.add("cpuWin");
	}
	history.appendChild(historyItemHTML);
	scrollHistoryToBottom();
}

function scrollHistoryToBottom() {
	history.scrollTop = history.scrollHeight;
}

function reloadGame() {
	let c = confirm("Restart game?\n\nAre you sure? Your progress will be lost.");
	if (c == true) {
		isStop = true;
		location.reload();
	}
}

export function restartGame() {
	cleanBeforeRound();
	historyText.classList.add("hidden");
	p1Deck = cpuDeck = p1Card = cpuCard = inRound = attribPlayed = isStop = player1Turn = waitingForNextCardPress = undefined;
	startGame();
}

function showPlayerNextCard() {
	if (inRound) return;
	cleanBeforeRound();
	inRound = true;
	getNextCards();

	p1CardSlot.appendChild(p1Card.getHTML());
	playAnimation();
	if (!player1Turn) {
		cpuPlayCard();
		growShrinkNextCardButton();
	} else {
		waitingForNextCardPress = false;
		makeOptionsActive();
	}
}

function showCPUCard() {
	cpuCardSlot.appendChild(cpuCard.getHTML(true));
	playAnimation(true);
	growShrinkNextCardButton();
}

function getNextCards() {
	p1Card = p1Deck.pop();
	cpuCard = cpuDeck.pop();
}

function isCardVisible(cpusCard = false) {
	if (cpusCard) return !!document.getElementById("cpuCard");
	return !!document.getElementById("p1Card");
}

function makeOptionsInactive() {
	for (let i = 0; i < playerOptionElements.length; i++) {
		playerOptionElements[i].classList.add("inactive");
	}
	p1Options.classList.add("hidden");
	nextCardButton.classList.remove("inactive");
	nextCardButtonContainer.classList.remove("hidden");
	optionsHeader.classList.add("hidden");
	buttonContainer.classList.add("transparent");
}

function makeOptionsActive() {
	for (let i = 0; i < playerOptionElements.length; i++) {
		playerOptionElements[i].classList.remove("inactive");
	}
	p1Options.classList.remove("hidden");
	nextCardButton.classList.add("inactive");
	nextCardButtonContainer.classList.add("hidden");
	optionsHeader.classList.remove("hidden");
	buttonContainer.classList.remove("transparent");
}

function showHideHistoryPopup() {
	historyContainer.classList.toggle("popup");
	scrollHistoryToBottom();
	overlayElement.classList.toggle("visible");
	showHistoryContainer.classList.toggle("hidden");
}

startGame();