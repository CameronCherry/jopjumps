// Deal with data from local file uploads.

import { restartGame } from "./app.js?rev=1";

const dataInput = document.getElementById("inputCSV");
const submitButton = document.getElementById("submitData");
let csvData = document.getElementById("csvData");
let oldcsvData = document.getElementById("csvData").innerHTML;
let isCSV = false;

dataInput.addEventListener("change", getFile);
submitButton.addEventListener("click", submitButtonClick);

function submitButtonClick() {
	if (!isCSV) {
		alert("An error occurred when submitting your data file.\n\nYour data file is not a .csv file. Please upload a .csv file and try again.");
		return;
	}
	if (csvData.innerHTML === oldcsvData) {
		alert("You just tried to submit your own data file.\n\nBut the new data is the same as the old data, so no changes have been made.");
		return;
	}
	while (csvData.innerHTML != oldcsvData) {
		submitButton.innerText = "Processing...";
		oldcsvData = document.getElementById("csvData").innerHTML;
	}
	submitButton.innerText = "Success!";
	submitButton.setAttribute("title", "Using local data");
	submitButton.setAttribute("disabled", "disabled");
	saveLocalStorage();
	restartGame();
}

function getFile(event) {
	const input = event.target;
	const parts = input.value.split('.');
	const extn = parts[parts.length - 1];
	if (extn.toLowerCase() === "csv") {
		isCSV = true;
		if ("files" in input && input.files.length > 0) {
			csvData.innerHTML = "";
			placeFileContent(csvData, input.files[0]);
		} else {
			alert("Error when importing data - did you select multiple files?");
		}
	} else {
		isCSV = false;
		alert("Error when selecting a file - did you select a file which isn't a .csv?");
	}
}

function placeFileContent(target, file) {
	readFileContent(file)
		.then((content) => {
			target.innerHTML = content;
		})
		.catch((error) => console.log(error));
}

function readFileContent(file) {
	const reader = new FileReader();
	return new Promise((resolve, reject) => {
		reader.onload = (event) => resolve(event.target.result);
		reader.onerror = (error) => reject(error);
		reader.readAsText(file);
	});
}

export function readLocalStorage() {
	const lsData = localStorage.getItem("localStorageCsvData");
	let csvData = document.getElementById("csvData");
	if (lsData) {
		console.log("Data saved from a previous upload - now using this data!");
		csvData.innerHTML = lsData;
	}
}

function saveLocalStorage() {
	localStorage.setItem("localStorageCsvData", csvData.innerHTML);
}
