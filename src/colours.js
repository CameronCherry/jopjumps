const html = document.querySelector("html");

function getLocalTheme() {
  return localStorage.getItem("theme");
}

function setLocalTheme() {
  const theme = getLocalTheme();
  setColourScheme(theme);
}

function setColourScheme(theme) {
    if (!theme) {
      theme = "light";
    }
    html.setAttribute("theme", theme);
    localStorage.setItem("theme", theme);
    console.log("Using " + theme + " theme");
}

function onThemeChange() {
  const colourDropdown = document.getElementById("colourSchemes");
  console.log("Changing theme...");
  const theme = colourDropdown.value;
  setColourScheme(theme);
}

function updateDropdownForCurrentTheme() {
  console.log("Updating colour dropdown for loaded local theme");
  const colourDropdown = document.getElementById("colourSchemes");
  const numColourOpts = colourDropdown.options.length;
  const theme = html.getAttribute("theme");
  for (let i=0; i<numColourOpts; i++){
    if (colourDropdown.options[i].value == theme){
      colourDropdown.options[i].selected = true;
      break;
    }
  }
}

setLocalTheme();