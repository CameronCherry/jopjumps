const hamburger = document.getElementById("hamburgerButton");
const sidebar = document.getElementById("sidebar");
const body = document.body;

function initAnimations() {

	body.addEventListener("click", function (elem) {
		hideSidebar(elem);
	});
	hamburger.addEventListener("click", showSidebar);
	
	let tl = anime.timeline({
		duration: 1000,
		loop: false,
		delay: function (el, i) {
			return i * 250;
		},
		easing: "easeInOutCubic",
	});

	tl.add(
		{
			targets: "#hamburgerSVG path",
			strokeDashoffset: [anime.setDashoffset, 0],
			duration: 1000,
		}
		)
		.add(
			{
				targets: "#restartButton path",
				strokeDashoffset: [anime.setDashoffset, 0],
				duration: 1000,
			},
			"-=1100"
			)
			.add(
				{
					targets: "#showHistoryButton path",
					strokeDashoffset: [anime.setDashoffset, 0],
					duration: 1000,
				},
		"-=1100"
		)
		.add(
			{
			targets: "#hamburgerSVG path",
			fill: "#000",
			duration: 500,
		},
		"-=1000"
		);
		
	}
		
		
function showSidebar() {
	console.log("showing sidebar");
	let tl = anime.timeline({
		duration: 300,
		loop: false,
		easing: "easeInOutCubic",
	});

	const sidebarid = "#sidebar";
	const hamburgerid = "#hamburgerButton";
	const sidebarChildren = sidebar.getElementsByTagName("*");
	if (sidebar.classList.contains("hidden")) {
		tl.add({
			targets: hamburgerid,
			rotateZ: -180,
			duration: 200,
		}).add(
			{
				targets: sidebarid,
				translateX: ["100%", "0%"],
			},
			"-=200"
		);
		sidebar.classList.remove("hidden");
		for (let item of sidebarChildren) {
			if (item.getAttribute("tabindex") === "-1") {
				item.setAttribute("tabindex", "0");
			}
		}
	} else {
		tl.add({
			targets: hamburgerid,
			rotateZ: 0,
			duration: 250,
		}).add(
			{
				targets: sidebarid,
				translateX: ["0%", "100%"],
			},
			"-=250"
		);
		sidebar.classList.add("hidden");

		for (let item of sidebarChildren) {
			if (item.getAttribute("tabindex") === "0") {
				item.setAttribute("tabindex", "-1");
			}
		}
	}
	tl.play();
}

function hideSidebar(elem) {
	if (
		elem.target.id === "hamburgerButton" ||
		elem.target.id === "hamburgerSVG" ||
		elem.target.id === "sidebar" ||
		elem.target.classList.contains("hamburgerPath") ||
		sidebar.contains(elem.target)
	)
		return;
	if (sidebar.classList.contains("hidden")) return;
	showSidebar();
}

export function playAnimation(showCPU = false) {
	let tl = anime.timeline({
		duration: 600,
		loop: false,
		easing: "easeInOutCubic",
	});

	let elemid = "#p1Card";
	if (showCPU) elemid = "#cpuCard";

	tl.add({
		targets: elemid,
		rotateY: [-90, 0],
		translateX: ["-100%", "-100%"],
	}).add({
		targets: elemid,
		translateX: ["-100%", "0%"],
	});

	tl.play();
	const cardElem = document.getElementById(elemid.substr(1));
	cardElem.addEventListener("mouseenter", function (elem) {
		onCardHover(false, elemid);
	});
	cardElem.addEventListener("pointerenter", function (elem) {
		onCardHover(false, elemid);
	});
	cardElem.addEventListener("mouseleave", function (elem) {
		onCardHover(true, elemid);
	});
	cardElem.addEventListener("pointerout", function (elem) {
		onCardHover(true, elemid);
	});
	return true;
}

export function growShrinkRestartButton() {
	let tl = anime.timeline({
		duration: 1000,
		loop: true,
		easing: "easeInElastic(1, .6)",
		direction: "alternate",
	});

	const elemid = "#restartButton";

	tl.add({
		targets: elemid,
		scale: 1.1,
	});

	tl.play();
	return true;
}

export function growShrinkNextCardButton() {
	let tl = anime.timeline({
		duration: 1000,
		delay: 2000,
		loop: true,
		easing: "easeInElastic(1, .9)",
		direction: "alternate",
	});

	const elemid = "#nextCardButton";

	tl.add({
		targets: elemid,
		scale: 0.95,
	});

	tl.play();
	return true;
}

function onCardHover(mouseLeaving = false, cardid) {
	let tl = anime.timeline({
		duration: 100,
		loop: false,
		easing: "easeInQuad",
	});

	if (mouseLeaving) {
		tl.add({
			targets: cardid,
			scale: 1.0,
		});
	} else {
		tl.add({
			targets: cardid,
			scale: 1.3,
		});
	}

	tl.play();
}

initAnimations();