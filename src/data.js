import { readLocalStorage } from "./localdata.js";
import { restartGame } from "./app.js?rev=1";

const defaultDataButton = document.getElementById("defaultData");
defaultDataButton.addEventListener("click", useDefaultData);

export function refreshData() {
	readLocalStorage();
	const csvData = document.getElementById("csvData");
	const csvDataHTML = csvData.innerHTML;

	const j = CSVToArray(csvDataHTML, ",");

	const ATTRIBUTES = j[0];
	const CARD_VALUES = j.slice(1, -1);

	CARD_VALUES[CARD_VALUES.length - 1].forEach((elem) => {
		return convertToDate(elem);
	});

	return { attributes: ATTRIBUTES, values: CARD_VALUES };
}

// ref: http://stackoverflow.com/a/1293163/2343
// This will parse a delimited string into an array of
// arrays. The default delimiter is the comma, but this
// can be overriden in the second argument.
function CSVToArray(strData, strDelimiter) {
	// Check to see if the delimiter is defined. If not,
	// then default to comma.
	strDelimiter = strDelimiter || ",";

	// Create a regular expression to parse the CSV values.
	let objPattern = new RegExp(
		// Delimiters.
		"(\\" +
			strDelimiter +
			"|\\r?\\n|\\r|^)" +
			// Quoted fields.
			'(?:"([^"]*(?:""[^"]*)*)"|' +
			// Standard fields.
			'([^"\\' +
			strDelimiter +
			"\\r\\n]*))",
		"gi"
	);

	// Create an array to hold our data. Give the array
	// a default empty first row.
	let arrData = [[]];

	// Create an array to hold our individual pattern
	// matching groups.
	let arrMatches = null;

	// Keep looping over the regular expression matches
	// until we can no longer find a match.
	while ((arrMatches = objPattern.exec(strData))) {
		// Get the delimiter that was found.
		let strMatchedDelimiter = arrMatches[1];

		// Check to see if the given delimiter has a length
		// (is not the start of string) and if it matches
		// field delimiter. If id does not, then we know
		// that this delimiter is a row delimiter.
		if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter) {
			// Since we have reached a new row of data,
			// add an empty row to our data array.
			arrData.push([]);
		}

		let strMatchedValue;

		// Now that we have our delimiter out of the way,
		// let's check to see which kind of value we
		// captured (quoted or unquoted).
		if (arrMatches[2]) {
			// We found a quoted value. When we capture
			// this value, unescape any double quotes.
			strMatchedValue = arrMatches[2].replace(new RegExp('""', "g"), '"');
		} else {
			// We found a non-quoted value.
			strMatchedValue = arrMatches[3];
		}

		// Now that we have our value string, let's add
		// it to the data array.
		arrData[arrData.length - 1].push(strMatchedValue);
	}

	// Return the parsed data.
	return arrData;
}

function roundArray(arr, dp) {
	return arr.map(function (element) {
		return Number(element.toFixed(dp));
	});
}

function convertToDate(dateStr) {
	const [day, month, year] = dateStr.split("/");
	return new Date(year, month - 1, day);
}

function useDefaultData() {
	if (
		confirm(
			"Use default JopJumps data?\n\nThis will restart the game. Are you sure you want to revert back to the default data?"
		)
	) {
		const DEFAULT_DATA =
			"Name,GamesPlayed,WinPerc,WinStreak,Debut\nJim,8,75,6,31/12/2020\nFred,12,33.3,4,31/01/2019\nBob,21,24.5,2,13/03/2021\nDave,45,80,12,08/12/2019\nGary,10,75,3,12/04/2020\nSteve,2,50,1,25/04/2021\nEdith,30,25,12,02/05/2018\nMarx,24,80,20,07/06/2018\nBart,12,16.6,1,04/05/2018\nAugustus,10,40,2,24/12/2018\nDarnell,1,0,0,23/07/2020\nJacqueline,8,12.5,1,07/12/2020\nDelfina,6,33.3,2,07/01/2020\nDonna,31,38.7,4,23/07/2020\nMelanie,16,75,8,13/03/2020\nAmelia,4,25,1,10/07/2018";
		const csvData = document.getElementById("csvData");
		csvData.innerHTML = DEFAULT_DATA;
		localStorage.removeItem("localStorageCsvData");
		restartGame();
	} else {
		console.log("Game not restarted. No change made to card data.");
	}
}
